

//2

/*If the url is http://localhost:4000/, send a response Welcome to Booking System
If the url is http://localhost:4000/profile, send a response Welcome to your profile!
If the url is http://localhost:4000/courses, send a response Here’s our courses available
If the url is http://localhost:4000/addcourse, send a response Add a course to our resources*/


	const http = require('http');

	let port = 4000;

	http.createServer(function(req, res){
		if(req.url === "/" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to the Booking System!");
				res.end();
			}
			else if(req.url === "/profile" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Welcome to your profile!");
				res.end();
			}
			else if(req.url === "/courses" && req.method === "GET"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Here are our available courses.");
				res.end();
			}
			else if(req.url === "/addCourse" && req.method === "POST"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Add a course to our resources.");
				res.end();
			}
			else if(req.url === "/updateCourse" && req.method === "PUT"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Update a course in our resources.");
				res.end();
			}
			else if(req.url === "/archiveCourse" && req.method === "DELETE"){
				res.writeHead(200, {'Content-Type': 'text/plain'});
				res.write("Archive a course in our resources.");
				res.end();
			} else {
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.write("Page Unavailable");
				res.end();
			}

	}).listen(port);

	console.log(`Server is running at port ${port}!`);


// 3
	// If the url is http://localhost:4000/updatecourse, send a response Update a course to our resources
	// If the url is http://localhost:4000/archivecourses, send a response Archive courses to our resources

	// same server

